#include "ScriptPCH.h"
#include "suffix.h"
#include "Player.h"
#include "DatabaseEnv.h"
#include "Chat.h"
#include "GossipDef.h"
#include "ScriptedGossip.h"
#include "WorldSession.h"
#include "PassiveAI.h"

void SuffixVendor::HandleSuffix(Player* player, Creature* creature, uint32 sender, uint32 entry)
{
    QueryResult result = CharacterDatabase.PQuery("SELECT DISTINCT entry, suffix, name FROM suffix where entry=%u ORDER BY name", entry);

    if (!result)
    {
        ChatHandler(player->GetSession()).PSendSysMessage("There is no available suffix for this item");
        return;
    }

    // ChatHandler(player->GetSession()).PSendSysMessage("You tried to purchase item id: %u", entry); // Were used for debug purposes

    /*Item* pItem = Item::CreateItem(entry, 1);
    ItemTemplate const* proto = pItem->GetTemplate();

    std::string iName = proto->Name1;*/ // Unwanted Feature

    //delete pItem;

    do {
        Field *fields = result->Fetch();
        uint32 item = fields[0].GetUInt32();
        uint32 suffix = fields[1].GetUInt32();
        std::string name = fields[2].GetString();

        std::stringstream buffer;

        buffer << name;

        AddGossipItemFor(player, 1, buffer.str(), suffix, entry);

    } while (result->NextRow());

    SendGossipMenuFor(player, DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
}

class NPC_suffix : public CreatureScript
{
public:
    NPC_suffix() : CreatureScript("SuffixVendor")
    {
    }
    struct suffix_npcAI : public PassiveAI
    {
        suffix_npcAI(Creature* creature) : PassiveAI(creature) { }
        bool GossipHello(Player* player) override
        {
            player->GetSession()->SendListInventory(me->GetGUID());
            return true;
        }

        bool GossipSelect(Player* player, uint32 /*menuId*/, uint32 gossipListId) override // Suffix = sender, Item entry = Action
        {
            uint32 const sender = player->PlayerTalkClass->GetGossipOptionSender(gossipListId);
            uint32 const action = player->PlayerTalkClass->GetGossipOptionAction(gossipListId);
            switch (action)
            {

            default:
            {
                player->PlayerTalkClass->ClearMenus();

                ItemPosCountVec dest;
                uint8 msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, action, 1);
                if (msg == EQUIP_ERR_OK)
                    player->StoreNewItem(dest, action, true, sender);

                CloseGossipMenuFor(player);
                break;
            }
            }
            return true;
        }
    };
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new suffix_npcAI(creature);
    }
};

void AddSC_NPC_suffix()
{
    new NPC_suffix();
}
