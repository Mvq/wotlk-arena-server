#include "Language.h"
#include "DBCStores.h"
#include "Log.h"
#include "Player.h"
#include "PassiveAI.h"
#include "ScriptedGossip.h"
#include "WorldSession.h"
#include "CreatureAI.h"


#define TXT_BAD_SKILL   "Profession NPC: received non-valid skill ID (LearnAllRecipesInProfession)"
#define TXT_ERR_SKILL   "you already have that skill"
#define TXT_PROBLEM     "Internal error occured!"
#define TXT_ERR_MAX     "you already know two professions!"

enum Gossip_Option_Custom
{
    CUSTOM_OPTION_NONE = 20,
    CUSTOM_OPTION_UNLEARN = 21,
    CUSTOM_OPTION_EXIT = 22,
    CUSTOM_OPTION_TITLE_PVP = 23,
    CUSTOM_OPTION_TITLE_CLIMB = 24,
    CUSTOM_OPTION_SPRINT = 25,
    GOSSIP_OPTION_HELLO = 26,
    CUSTOM_OPTION_ITEM_MENU = 27,
    CUSTOM_OPTION_ITEM_MENU_P2 = 28,
    CUSTOM_OPTION_ITEM_MENU_P3 = 29,
    CUSTOM_OPTION_ITEM_MENU_P4 = 30,
    CUSTOM_OPTION_ITEM_MENU_P5 = 31,
    CUSTOM_OPTION_ITEM_MENU_P6 = 32,
    CUSTOM_OPTION_ITEM_MENU_P7 = 33,
    CUSTOM_OPTION_ITEM_MENU_MAX = 34,
    CUSTOM_OPTION_SUFFIX = 35,
    CUSTOM_OPTION_PROPERTY = 36,
    CUSTOM_OPTION_VENDOR = 37,
    CUSTOM_OPTION_MAX
};

class profession_npc : public CreatureScript
{
public:
    profession_npc() : CreatureScript("profession_npc") { }
    struct profession_npcAI : public PassiveAI
    {
        profession_npcAI(Creature* creature) : PassiveAI(creature) { }
        bool LearnAllRecipesInProfession(Player* player, SkillType skill)
        {
            SkillLineEntry const* SkillInfo = sSkillLineStore.LookupEntry(skill);
            uint16 skill_level = 225;
            if (!SkillInfo)
            {
                TC_LOG_ERROR("entitles.player.skills", TXT_BAD_SKILL);
                return false;
            }
            if (skill == SKILL_ENGINEERING)
                skill_level = 150;
            player->SetSkill(SkillInfo->id, player->GetSkillStep(SkillInfo->id), skill_level, skill_level);

            return true;
        }

        bool ForgotSkill(Player* player, SkillType skill)
        {
            if (!player->HasSkill(skill))
                return false;
            SkillLineEntry const* SkillInfo = sSkillLineStore.LookupEntry(skill);
            if (!SkillInfo)
            {
                TC_LOG_ERROR("entitles.player.skills", TXT_BAD_SKILL);
                return false;
            }
            player->SetSkill(SkillInfo->id, player->GetSkillStep(SkillInfo->id), 0, 0);

            return true;
        }

        bool PlayerAlreadyHasTwoProfessions(Player* player)
        {
            uint32 skillCount = player->HasSkill(SKILL_MINING) + player->HasSkill(SKILL_SKINNING)
                + player->HasSkill(SKILL_HERBALISM) + player->HasSkill(SKILL_ENGINEERING);
            if (skillCount >= 2)
                return true;

            return false;
        }

        void CompleteLearnProfession(Player* player, SkillType skill)
        {
            if (PlayerAlreadyHasTwoProfessions(player))
                player->GetSession()->SendNotification(TXT_ERR_MAX);
            else if (!LearnAllRecipesInProfession(player, skill))
                player->GetSession()->SendNotification(TXT_PROBLEM);
        }

        bool GossipHello(Player* player) override
        {
            if (!player || !me)
                return false;

            if (!player->HasSkill(SKILL_ENGINEERING))
            {
                AddGossipItemFor(player, GOSSIP_ICON_TRAINER, "Engineering", GOSSIP_SENDER_MAIN, SKILL_ENGINEERING);
            }
            if (!player->HasSkill(SKILL_HERBALISM))
            {
                AddGossipItemFor(player, GOSSIP_ICON_TRAINER, "Herbalism", GOSSIP_SENDER_MAIN, SKILL_HERBALISM);
            }
            if (!player->HasSkill(SKILL_SKINNING))
            {
                AddGossipItemFor(player, GOSSIP_ICON_TRAINER, "Skinning", GOSSIP_SENDER_MAIN, SKILL_SKINNING);
            }
            if (!player->HasSkill(SKILL_MINING))
            {
                AddGossipItemFor(player, GOSSIP_ICON_TRAINER, "Mining", GOSSIP_SENDER_MAIN, SKILL_MINING);
            }
            AddGossipItemFor(player, GOSSIP_ICON_INTERACT_1, "Unlearn All", CUSTOM_OPTION_UNLEARN, 0);
            SendGossipMenuFor(player, 59898, me->GetGUID());
            return true;
        }

        bool GossipSelect(Player* player, uint32 /*menuId*/, uint32 gossipListId) override
        {
            uint32 const skill = player->PlayerTalkClass->GetGossipOptionAction(gossipListId);
            uint32 const sender = player->PlayerTalkClass->GetGossipOptionSender(gossipListId);

            ClearGossipMenuFor(player);
            if (sender == CUSTOM_OPTION_UNLEARN)
            {
                ForgotSkill(player, SKILL_ENGINEERING);
                ForgotSkill(player, SKILL_HERBALISM);
                ForgotSkill(player, SKILL_SKINNING);
                ForgotSkill(player, SKILL_MINING);
            }
            else if (sender == GOSSIP_SENDER_MAIN)
            {
                if (player->HasSkill(skill))
                    player->GetSession()->SendNotification(TXT_ERR_SKILL);
                else
                    CompleteLearnProfession(player, (SkillType)skill);
            }
            if (sender == CUSTOM_OPTION_EXIT)
                player->PlayerTalkClass->SendCloseGossip();
            else
                GossipHello(player);

            return true;
        }
    };
    CreatureAI* GetAI(Creature* creature) const override
    {
        return new profession_npcAI(creature);
    }
};

void AddSC_cyclone_customs()
{
    new profession_npc();
}
